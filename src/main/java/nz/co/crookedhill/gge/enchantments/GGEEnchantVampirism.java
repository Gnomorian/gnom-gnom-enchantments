package nz.co.crookedhill.gge.enchantments;

import java.util.Map;
import java.util.Map.Entry;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.enchantment.EnumEnchantmentType;
import net.minecraft.entity.EntityLivingBase;
import net.minecraftforge.event.entity.living.LivingAttackEvent;
import nz.co.crookedhill.gge.helper.GGEAbstractEnchantment;
import nz.co.crookedhill.gge.helper.GGEConfigManager;
/*
 * when the player attacks someone, this enchantment transfers
 * the life lost from the enemy to the attacker.
 */
public class GGEEnchantVampirism extends GGEAbstractEnchantment
{
	public static final int weight = 3;
	public static final EnumEnchantmentType enchantType = EnumEnchantmentType.weapon;

	public GGEEnchantVampirism(int effectID) 
	{
		super(effectID, GGEEnchantVampirism.enchantType);
	}

	public class Handler
	{
		@SubscribeEvent
		public void onAttackedEvent(LivingAttackEvent event)
		{
			if(!event.entity.worldObj.isRemote)
			{
				if(event.source.getEntity() instanceof EntityLivingBase)
				{
					EntityLivingBase attacker = (EntityLivingBase)event.source.getEntity();
					if(attacker.getHeldItem() != null && attacker.getHeldItem().getEnchantmentTagList() != null)
					{
						Map<Integer, Integer> enchants = EnchantmentHelper.getEnchantments(attacker.getHeldItem());
						for(Entry entry : enchants.entrySet())
						{
							System.out.println("ID:" + entry.getKey() + " LVL:" + entry.getValue());
							if((Integer)entry.getKey() == Enchantment.sharpness.effectId)
							{
								if(entry.getKey().equals(GGEConfigManager.instance.vampireID))
								{
									System.out.println("user wields a weapon with vampirism enchantment");
								}
							}
						}
					}
				}
			}
		}
	}
}
