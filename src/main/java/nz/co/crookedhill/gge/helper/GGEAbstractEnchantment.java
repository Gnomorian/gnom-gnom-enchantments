package nz.co.crookedhill.gge.helper;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnumEnchantmentType;
import net.minecraftforge.common.MinecraftForge;

public abstract class GGEAbstractEnchantment extends Enchantment
{
	
	protected GGEAbstractEnchantment(int weight,EnumEnchantmentType type) 
	{
		super(getNextEnchantmentSlot(), weight, type);
		
	}
	
	public abstract class Handler{	}
	
	private static int getNextEnchantmentSlot()
	{
		int j = 200;
		for(int i=100; i<Enchantment.enchantmentsList.length-100; i++)
		{
			if(Enchantment.enchantmentsList[i] == null)
			{
				return i;
			}
		}
		return j;
	}
}

