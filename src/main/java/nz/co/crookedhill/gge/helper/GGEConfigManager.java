package nz.co.crookedhill.gge.helper;

import java.util.HashMap;

import net.minecraft.enchantment.Enchantment;
import net.minecraftforge.common.config.Configuration;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;

public class GGEConfigManager 
{
	// map of ids
	private HashMap<String, Integer> enchantIDMap = new HashMap<String, Integer>();
	//the id after vanilla ids have finished.
	int minID = 50;
	// max is 250, but 251=don't load the enchantment.
	int maxID = 251;
	// ENCHANTMENT IDS
	public int vampireID = 200;
	
	public static GGEConfigManager instance;
	
	public void init(FMLPreInitializationEvent event)
	{
		instance = this;
		
		Configuration config = new Configuration(event.getSuggestedConfigurationFile());
		String idMessage = "if you set the id to 251 the enchantment will not exist in the game.";
		
		vampireID = config.getInt("Vampirism Enchantment", "Enchants", vampireID, 50, 251, idMessage);
		
		
		try{
			config.load();
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
			config.getInt("Vampirism ID", "Enchantment IDs", this.vampireID, 50, 251, idMessage);
		}
		finally
		{
			config.save();
		}
	}
	private void registerEnchantIDs(HashMap<String, Integer> idMap)
	{
		
	}
	public int getEnchantID(String wantedEnchantment)
	{
		return (int)enchantIDMap.get(wantedEnchantment);
	}
	/**
	 * 	returns an array of all the free enchantment ids.
	 * @return int[] of enchantment ids
	 */
	public int[] getUnregisteredIDs()
	{
		/* number of enchantment ids that are free. */
		int freeEnchantids = 0;
		
		/* the free enchantment ids themselves. */
		int[] freeidNumbers = new int[Enchantment.enchantmentsList.length];
		/* gets all the free enchantment ids, and stores them and how many there are */
		for(int i = 0; i < Enchantment.enchantmentsList.length; i++)
		{
			if(Enchantment.enchantmentsList[i] == null)
			{
				freeidNumbers[freeEnchantids] = i;
				freeEnchantids++;
			}
		}
		/* condenced list of all the free ids to be returned with no null spaces */
		int[] ids = new int[freeEnchantids];
		
		/* stores all the free ids in a condenced array to save on checking for null spaces */
		for(int i = 0; i < freeEnchantids; i++)
		{
			ids[i] = freeidNumbers[i];
		}
		return ids;
	}
}
