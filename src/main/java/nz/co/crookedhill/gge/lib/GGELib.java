package nz.co.crookedhill.gge.lib;

public class GGELib 
{
	/* MOD STRINGS */
	public static final String NAME = "GnomGnom's Enchants";
	public static final String VERSION = "0.0.0.0";
	public static final String MOD_ID = "ggenchants";
	public static final String CLIENT_PROXY = "nz.co.crookedhill.gge.proxy.CommonProxy";
	public static final String SERVER_PROXY = "nz.co.crookedhill.gge.proxy.ClientProxy";
	
	
	/* ENCHANTMENT NAMES */
	public static final String ENCHANTMENT_VAMPIRISM = "vampireEnchantment";
}
