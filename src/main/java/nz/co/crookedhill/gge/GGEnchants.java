package nz.co.crookedhill.gge;

import net.minecraft.enchantment.Enchantment;
import net.minecraftforge.common.MinecraftForge;
import nz.co.crookedhill.gge.handlers.GGEHandlerVampirism;
import nz.co.crookedhill.gge.helper.GGEAbstractEnchantment;
import nz.co.crookedhill.gge.helper.GGEConfigManager;
import nz.co.crookedhill.gge.lib.GGELib;
import nz.co.crookedhill.gge.proxy.CommonProxy;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;

@Mod(useMetadata=true, modid=GGELib.MOD_ID,name=GGELib.NAME, version=GGELib.VERSION)
public class GGEnchants 
{
	@Instance(value=GGELib.MOD_ID)
	public static GGEnchants instance;

	@SidedProxy(serverSide=GGELib.SERVER_PROXY, clientSide=GGELib.CLIENT_PROXY)
	public static CommonProxy proxy;

	@EventHandler
	public void preInit(FMLPreInitializationEvent event)
	{
		new GGEConfigManager().init(event);
	}
	@EventHandler
	public void postInit(FMLPostInitializationEvent event)
	{
		MinecraftForge.EVENT_BUS.register(new GGEHandlerVampirism());
	}
	
	private void registerEnchantment(GGEAbstractEnchantment enchantment, Object...handlers)
	{
		Enchantment.addToBookList(enchantment);
	}
}